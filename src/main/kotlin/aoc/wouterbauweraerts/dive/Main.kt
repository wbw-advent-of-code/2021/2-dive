package aoc.wouterbauweraerts.dive

fun main(args: Array<String>) {
    val fileName = "src/main/resources/input.txt"

    val finalPosition = PositionCalculator().calculateFinal(fileName)
    println(finalPosition.hPos * finalPosition.depth)
}