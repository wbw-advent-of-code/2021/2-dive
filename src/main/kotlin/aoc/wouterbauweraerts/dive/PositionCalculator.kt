package aoc.wouterbauweraerts.dive

import java.io.File

class PositionCalculator {
    fun calculate(fileName: String): Position {
        return Position().applyAll(
        File(fileName).useLines { it.toList() }
            .map { it.toDiveCommand() }
        )
    }

    fun calculateFinal(fileName: String): Position {
        return Position().applyAllPt2(
            File(fileName).useLines { it.toList() }
                .map { it.toDiveCommand() }

        )
    }

}