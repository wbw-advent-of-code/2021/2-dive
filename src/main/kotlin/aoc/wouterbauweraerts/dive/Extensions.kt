package aoc.wouterbauweraerts.dive

fun String.toDiveCommand(): DiveCommand {
    val partials = this.split(" ")
    return DiveCommand(
        direction = Direction.valueOf(partials[0].uppercase()),
        amount = partials[1].toInt()
    )
}