package aoc.wouterbauweraerts.dive

data class Position(val hPos: Int = 0, val depth: Int = 0, private val aim: Int = 0) {
    fun up(amount: Int): Position {
        val newVal = if (this.depth - amount < 0) {
            0
        } else {
            this.depth - amount
        }

        return this.copy(depth = newVal)
    }

    fun down(amount: Int): Position {
        return this.copy(depth = this.depth + amount)
    }

    fun fwd(amount: Int): Position {
        return this.copy(hPos = this.hPos + amount)
    }

    private fun fwd2(amount: Int): Position {
        return this.copy(hPos = this.hPos + amount, depth= this.depth + amount * this.aim)
    }

    fun apply(cmd: DiveCommand): Position {
        return when (cmd.direction) {
            Direction.UP -> up(cmd.amount)
            Direction.DOWN -> down(cmd.amount)
            Direction.FORWARD -> fwd(cmd.amount)
        }
    }

    fun applyAll(commands: List<DiveCommand>): Position {
        return if (commands.isNotEmpty()) {
            this.apply(commands[0]).applyAll(commands.drop(1))
        } else {
            this
        }
    }

    fun applyAllPt2(commands: List<DiveCommand>): Position {
        return if (commands.isNotEmpty()) {
            this.applyPt2(commands[0]).applyAllPt2(commands.drop(1))
        } else {
            this
        }
    }

    private fun applyPt2(cmd: DiveCommand): Position {
        return when (cmd.direction) {
            Direction.UP -> aim(cmd.amount * -1)
            Direction.DOWN -> aim(cmd.amount)
            Direction.FORWARD -> fwd2(cmd.amount)
        }
    }


    private fun aim(amount: Int): Position {
        return this.copy(aim = this.aim + amount)
    }
}

data class DiveCommand(val direction: Direction, val amount: Int)

enum class Direction {
    UP, DOWN, FORWARD
}