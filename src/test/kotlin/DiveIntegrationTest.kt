import aoc.wouterbauweraerts.dive.PositionCalculator
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class DiveIntegrationTest {
    private val fileName = "src/test/resources/input.txt"

    lateinit var calc: PositionCalculator

    @BeforeEach
    internal fun setUp() {
        calc = PositionCalculator()
    }

    @Test
    internal fun `calculate returns expected position`() {
        val result = calc.calculate(fileName)
        assertEquals(15, result.hPos)
        assertEquals(10, result.depth)
    }

    @Test
    internal fun `calculateFinal returns expected position`() {
        val result = calc.calculateFinal(fileName)
        assertEquals(15, result.hPos)
        assertEquals(60, result.depth)
    }
}
