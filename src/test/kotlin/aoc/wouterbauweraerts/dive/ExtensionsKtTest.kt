package aoc.wouterbauweraerts.dive

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.EnumSource
import kotlin.random.Random

internal class ExtensionsKtTest {
    @ParameterizedTest
    @EnumSource(Direction::class)
    internal fun testToDiveCommand(direction: Direction) {
        repeat(5) {
            Random.Default.let {
                val amount = it.nextInt()
                val stringToParse = "$direction $amount"

                assertEquals(DiveCommand(direction, amount), stringToParse.toDiveCommand())
            }
        }
    }
}