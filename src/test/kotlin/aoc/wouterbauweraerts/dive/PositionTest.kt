package aoc.wouterbauweraerts.dive

import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream
import kotlin.test.assertEquals

internal class PositionTest {

    @ParameterizedTest
    @MethodSource("upSrc")
    internal fun `test Position up`(initHpos: Int, initDepth: Int, amount: Int, expectedDepth: Int) {
        val pos = Position(hPos = initHpos, depth = initDepth)
        assertEquals(expectedDepth, pos.up(amount).depth)
    }

    @ParameterizedTest
    @MethodSource("downSrc")
    internal fun `test Position down`(initHpos: Int, initDepth: Int, amount: Int, expectedDepth: Int) {
        val pos = Position(hPos = initHpos, depth = initDepth)
        assertEquals(expectedDepth, pos.down(amount).depth)
    }

    @ParameterizedTest
    @MethodSource("fwdSrc")
    internal fun `test Position forward`(initDepth: Int, initHpos: Int, amount: Int, expectedHpos: Int) {
        val pos = Position(hPos = initHpos, depth = initDepth)
        assertEquals(expectedHpos, pos.fwd(amount).hPos)
    }

    @ParameterizedTest
    @MethodSource("applySrc")
    internal fun testApplyDiveCommand(position: Position, cmd: DiveCommand, expected: Position) {
        assertEquals(expected, position.apply(cmd))
    }

    companion object {
        @JvmStatic
        fun upSrc(): Stream<Arguments> {
            return Stream.of(
                Arguments.of(0,0,0,0),
                Arguments.of(0,0,1,0),
                Arguments.of(0,0,2,0),
                Arguments.of(0,1,0,1),
                Arguments.of(0,1,1,0),
                Arguments.of(0,1,2,0),
                Arguments.of(0,2,0,2),
                Arguments.of(0,2,1,1),
                Arguments.of(0,2,2,0)
            )
        }

        @JvmStatic
        fun downSrc(): Stream<Arguments> {
            return Stream.of(
                Arguments.of(0,0,0,0),
                Arguments.of(0,0,1,1),
                Arguments.of(0,0,2,2),
                Arguments.of(0,1,0,1),
                Arguments.of(0,1,1,2),
                Arguments.of(0,1,2,3),
                Arguments.of(0,2,0,2),
                Arguments.of(0,2,1,3),
                Arguments.of(0,2,2,4)
            )
        }

        @JvmStatic
        fun fwdSrc(): Stream<Arguments> {
            return Stream.of(
                Arguments.of(0,0,0,0),
                Arguments.of(0,0,1,1),
                Arguments.of(0,0,2,2),
                Arguments.of(0,1,0,1),
                Arguments.of(0,1,1,2),
                Arguments.of(0,1,2,3),
                Arguments.of(0,2,0,2),
                Arguments.of(0,2,1,3),
                Arguments.of(0,2,2,4)
            )
        }

        @JvmStatic
        fun applySrc(): Stream<Arguments> {
            return Stream.of(
                Arguments.of(Position(), DiveCommand(Direction.FORWARD, 5), Position(hPos = 5)),
                Arguments.of(Position(), DiveCommand(Direction.UP, 5), Position()),
                Arguments.of(Position(), DiveCommand(Direction.DOWN, 5), Position(depth = 5)),
                Arguments.of(Position(depth = 8), DiveCommand(Direction.UP, 3), Position(depth = 5)),
                Arguments.of(Position(8,6), DiveCommand(Direction.FORWARD, 5), Position(hPos = 13, depth = 6))
            )
        }
    }
}